﻿/* Versioner.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 * Manages version/bundle numbering
 */
 
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;

public class Versioner : EditorWindow {
    static VersionInfo versionAsset;
    
    [MenuItem( "Versioner/Set Version" )]
    static void Init() {
        EditorWindow.GetWindow<Versioner>( "Version Number" );
    }

    void OnGUI() {
        if ( !versionAsset )
            GetVersions();

        bool updateAsset = false;
        var newMaj = EditorGUILayout.IntField( "Major Version", versionAsset.majorVersion );
        if ( newMaj != versionAsset.majorVersion ) {
            versionAsset.majorVersion = newMaj;
            updateAsset = true;
        }
        var newMin = EditorGUILayout.IntField( "Minor Version", versionAsset.minorVersion );
        if ( newMin != versionAsset.minorVersion ) {
            versionAsset.minorVersion = newMin;
            updateAsset = true;
        }
        var newRevision = EditorGUILayout.IntField( "Revision", versionAsset.revision );
        if ( newRevision != versionAsset.revision ) {
            versionAsset.revision = newRevision;
            updateAsset = true;
        }

        if ( updateAsset )
            UpdateVersionAsset();
    }

    [UnityEditor.Callbacks.PostProcessBuild]
    public static void OnBuilt( BuildTarget buildTarget, string buildFolder ) {
        Debug.Log( buildTarget + " at " + buildFolder );
        IncrementMinorVersion();
    }
    
    [MenuItem( "Versioner/Increment Minor Version")]
    public static void IncrementMinorVersion() {
        if ( !versionAsset )
            GetVersions();

        versionAsset.minorVersion++;
        versionAsset.revision = 0;
        UpdateVersionAsset();
    }

    public static void IncrementRevision() {
        if ( !versionAsset )
            GetVersions();

        versionAsset.revision++;
        UpdateVersionAsset();
    }

    static void UpdateVersionAsset() {
        PlayerSettings.bundleVersion = string.Format( "{0}.{1:D2}.{2:D4}", versionAsset.majorVersion, versionAsset.minorVersion, versionAsset.revision );
        EditorUtility.SetDirty( versionAsset );
        AssetDatabase.SaveAssets();
        EditorApplication.SaveAssets();
    }

    static void GetVersions() {
        versionAsset = AssetDatabase.LoadAssetAtPath( "Assets/Config/versionInfo.asset", typeof( VersionInfo ) ) as VersionInfo;
        if ( !versionAsset ) {
            versionAsset = ScriptableObject.CreateInstance<VersionInfo>();
            versionAsset.name = "Version Info";
            AssetDatabase.CreateFolder( "Assets", "Config" );
            AssetDatabase.CreateAsset( versionAsset, "Assets/Config/versionInfo.asset" );
        }
    }
}
