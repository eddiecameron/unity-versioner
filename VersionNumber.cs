﻿/* VersionNumber.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 * Just displays a version number
 * based on http://xeophin.net/en/blog/2014/05/09/simple-version-numbering-unity3d
 * 
 * Place on an object in your startup scene
 */

using UnityEngine;
using System.Reflection;

public class VersionNumber : MonoBehaviour {
    public VersionInfo versionInfo;

    /// <summary>
    /// Can be set to true, in that case the version number will be shown in bottom right of the screen
    /// </summary>
    public bool ShowVersionInformation = false;
    /// <summary>
    /// Show the version during the first 20 seconds.
    /// </summary>
    public bool ShowVersionDuringTheFirst20Seconds = true;
    string version;
    Rect position = new Rect( 0, 0, 200, 40 );

    /// <summary>
    /// Gets the version.
    /// </summary>
    /// <value>The version.</value>
    public string Version {
        get {
            if ( version == null ) {
                version = versionInfo ? string.Format( "{0}.{1:D2}.{2:D4}", versionInfo.majorVersion, versionInfo.minorVersion, versionInfo.revision ) : "NO VERSION INFO FOUND";
            }
            return version;
        }
    }


    /// Use this for initialization
    void Start() {
        DontDestroyOnLoad( this );
        
        // Log current version in log file
        Debug.Log( string.Format( "Currently running version is {0}", Version ) );

        if ( ShowVersionDuringTheFirst20Seconds ) {
            ShowVersionInformation = true;
            Destroy( this, 20f );
        }

        position.x = Screen.width - position.width - 10f;
        position.y = 10f;

    }


    void OnGUI() {
        if ( !ShowVersionInformation ) {
            return;
        }

        var style = GUI.skin.GetStyle( "label" );
        style.fontSize = 24;
        GUI.contentColor = Color.gray;
        GUI.Label( position, string.Format( "v{0}", Version ) );
    }
}