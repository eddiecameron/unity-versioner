﻿/* VersionInfo.cs
 * Copyright Eddie Cameron 2015
 * ----------------------------
 * Just holds three numbers for versioning
 */

using UnityEngine;
using System.Collections;

public class VersionInfo : ScriptableObject {
    public int majorVersion, minorVersion, revision;
}
